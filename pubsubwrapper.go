package pubsubwrapper

import (
	"cloud.google.com/go/pubsub"
	"context"
	"fmt"
	"github.com/gofrs/uuid"
	"sync"
	"time"
)

var (
	ErrSubNotFound   = fmt.Errorf("sub not found")
	ErrTopicNotFound = fmt.Errorf("topic not found")
	ErrCloseClient   = fmt.Errorf("failed to close gcpClient")
)

type GooglePubSubWrapper struct {
	client    *pubsub.Client
	getSub    map[string]*pubsub.Subscription
	mu        sync.Mutex
	subName   string
	subPrefix string
}

func NewClient(projectID, subName string) (*GooglePubSubWrapper, error) {
	if projectID == "" {
		return nil, fmt.Errorf("project id must be set")
	}

	client, err := pubsub.NewClient(context.Background(), projectID)
	if err != nil {
		return nil, err
	}

	return &GooglePubSubWrapper{
		client:  client,
		subName: subName,
		getSub:  make(map[string]*pubsub.Subscription),
	}, nil
}

func (g *GooglePubSubWrapper) SetSubPrefix(subPrefix string) {
	g.subPrefix = subPrefix
}

func (g *GooglePubSubWrapper) CloseClient() error {
	err := g.client.Close()
	if err != nil {
		return ErrCloseClient
	}

	return nil
}

func (g *GooglePubSubWrapper) CreateTopicOrGet(ctx context.Context, topicName string) (*pubsub.Topic, error) {
	if ctx == nil {
		ctx = context.Background()
	}

	topic := g.client.Topic(topicName)
	if isExist, err := topic.Exists(ctx); err != nil {
		return nil, err
	} else if err == nil && isExist {
		return topic, nil
	}

	return g.client.CreateTopic(ctx, topicName)
}

func (g *GooglePubSubWrapper) DeleteTopic(ctx context.Context, topic *pubsub.Topic) error {
	if ctx == nil {
		ctx = context.Background()
	}
	return topic.Delete(ctx)
}

func (g *GooglePubSubWrapper) DeleteSub(ctx context.Context, subName string) error {
	if ctx == nil {
		ctx = context.Background()
	}

	g.mu.Lock()
	if sub, ok := g.getSub[subName]; ok {
		g.mu.Unlock()
		return sub.Delete(ctx)
	}
	g.mu.Unlock()
	return ErrSubNotFound
}

func (g *GooglePubSubWrapper) CreateSub(ctx context.Context, topic *pubsub.Topic) (*pubsub.Subscription, error) {
	if ctx == nil {
		ctx = context.Background()
	}

	getUUID, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	subName := fmt.Sprintf("%s-%s", g.subPrefix, getUUID.String())
	sub, err := g.client.CreateSubscription(ctx, subName, pubsub.SubscriptionConfig{
		Topic:       topic,
		AckDeadline: 20 * time.Second,
	})
	if err != nil {
		return nil, err
	}

	g.mu.Lock()
	g.getSub[sub.String()] = sub
	g.mu.Unlock()

	return sub, nil
}

func (g *GooglePubSubWrapper) GetTopic(ctx context.Context, topicName string) (*pubsub.Topic, error) {
	if ctx == nil {
		ctx = context.Background()
	}

	topic := g.client.Topic(topicName)
	if isExist, err := topic.Exists(ctx); err != nil {
		return nil, err
	} else if err == nil && !isExist {
		return nil, ErrTopicNotFound
	}

	return topic, nil
}

func (g *GooglePubSubWrapper) GetSubscription(ctx context.Context, topic *pubsub.Topic) (*pubsub.Subscription, error) {
	if ctx == nil {
		ctx = context.Background()
	}

	sub := g.client.Subscription(g.subName)
	if ok, _ := sub.Exists(ctx); !ok {
		return nil, ErrSubNotFound
	}

	g.mu.Lock()
	g.getSub[sub.String()] = sub
	g.mu.Unlock()

	return sub, nil
}

func (g *GooglePubSubWrapper) PullMessage(ctx context.Context, subName string, topic *pubsub.Topic, callback func(msg *pubsub.Message)) error {
	if ctx == nil {
		ctx = context.Background()
	}

	g.mu.Lock()

	if sub, ok := g.getSub[subName]; ok {
		g.mu.Unlock()
		return sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
			callback(msg)
			msg.Ack()
		})
	}

	g.mu.Unlock()
	return ErrSubNotFound
}

func (g *GooglePubSubWrapper) Publish(ctx context.Context, topic *pubsub.Topic, message string, attrs map[string]string) (string, error) {
	if ctx == nil {
		ctx = context.Background()
	}
	return topic.Publish(ctx, &pubsub.Message{
		Data:       []byte(message),
		Attributes: attrs,
	}).Get(ctx)
}
